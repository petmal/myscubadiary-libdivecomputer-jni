/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#include "Dive.h"

using namespace std;

Dive::Dive(const AutoParser & parser)
	: gases(parser.getGasMixes()),
	tanks(parser.getTanks()),
	avgDepth(parser.getAvgDepth()),
	bottomTemperature(parser.getBottomTemperature()),
	dateTime(parser.getDateTime()),
	diveTime(parser.getDiveTime()),
	maxDepth(parser.getMaxDepth()),
	profile(parser.getDiveProfile()) {}

const dc_gasmix_t * Dive::getPrimaryGasMix() const {
	const unsigned int numGases = gases.size();
	const dc_tank_t * primaryTank = findPrimaryGasTank();
	const dc_gasmix_t * primaryGasMix = 0;
	if (primaryTank != 0) {
		const unsigned int mixIndex = primaryTank->gasmix;
		if ((mixIndex != DC_GASMIX_UNKNOWN) && (mixIndex < numGases)) {
			primaryGasMix = &gases[mixIndex];
		}
	} else if (numGases > 0) {
		primaryGasMix = &gases[0];
	}

	if ((primaryGasMix != 0) && (primaryGasMix->oxygen > 0)) {
		return primaryGasMix;
	}

	return 0;
}

const dc_tank_t * Dive::getPrimaryGasTank() const {
	const dc_tank_t * primaryTank = findPrimaryGasTank();
	if ((primaryTank != 0) && (primaryTank->type != DC_TANKVOLUME_NONE) && (primaryTank->volume > 0) && (primaryTank->workpressure > 0)) {
		return primaryTank;
	}

	return 0;
}

const dc_tank_t * Dive::findPrimaryGasTank() const {
	if (!tanks.empty()) {
		return &tanks[0];
	}

	return 0;
}

double Dive::getUsedPressure() const {
	const dc_tank_t * primaryTank = findPrimaryGasTank();
	if (primaryTank != 0) {
		const double usedPressure = primaryTank->beginpressure - primaryTank->endpressure;
		if (usedPressure > 0) {
			return usedPressure;
		}
	}

	return 0;
}

double Dive::getAvgDepth() const {
	const double * const depth = avgDepth.get();
	if (depth != 0) {
		return *depth;
	}

	return 0;
}

const double * Dive::getBottomTemperature() const {
	return bottomTemperature.get();
}