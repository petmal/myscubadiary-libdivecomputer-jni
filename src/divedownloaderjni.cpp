/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#include "divedownloaderjni.h"

using namespace std;

namespace {

	AutoDevice * attachedDevice;

	const char * dateTimeToTimestamp(const dc_datetime_t & dateTime, char (&buffer)[20]) {
		sprintf(buffer, "%04i-%02i-%02i %02i:%02i:%02i", dateTime.year, dateTime.month, dateTime.day,
			dateTime.hour, dateTime.minute, dateTime.second);
		return buffer;
	}

	jint roundToInt(const double value) {
		return floor(value + 0.5);
	}

	vector<shared_ptr<float>> replaceNegativeWithZero(const vector<shared_ptr<float>> & numbers) {
		vector<shared_ptr<float>> processed(numbers.size());
		replace_copy_if(numbers.begin(), numbers.end(), processed.begin(), [](const shared_ptr<float> & nativePointer){
			return ((nativePointer != 0) && (*nativePointer <= 0.0f));
		}, make_shared<float>(0.0f));

		return processed;
	}

	set<unsigned int> findIndexesWithNullValue(const vector<shared_ptr<float>> & numbers) {
		const unsigned int size = numbers.size();
		set<unsigned int> indexesWithNullValue;
		for (unsigned int index = 0; index < size; ++index) {
			const shared_ptr<float> & valuePointer = numbers[index];
			if (valuePointer == 0) {
				indexesWithNullValue.insert(index);
			}
		}

		return indexesWithNullValue;
	}

	jobject handleJavaException(JNIEnv * java, jclass wrapperClazz, jmethodID wrapperConstructor, const char * message) {
		if (wrapperClazz != 0) {
			jthrowable cause = java->ExceptionOccurred();
			java->ExceptionClear();
			jthrowable wrappedThrowable = wrapJavaThrowable(java, wrapperClazz, wrapperConstructor, message, cause);
			if (wrappedThrowable != 0) {
				java->Throw(wrappedThrowable);
			} else {
				return throwExceptionMessage(java, wrapperClazz, message);
			}
		}

		return 0;
	}

	jobject throwExceptionMessage(JNIEnv * java, jclass wrapperClazz, const char * message) {
		if (wrapperClazz != 0) {
			java->ExceptionClear();
			java->ThrowNew(wrapperClazz, message);
		}

		return 0;
	}

	jthrowable wrapJavaThrowable(JNIEnv * java, jclass wrapperClazz, jmethodID wrapperConstructor, const char * message, jthrowable cause) {
		if ((wrapperClazz != 0) && (wrapperConstructor != 0)) {
			try {
				jstring messageText = java->NewStringUTF(message);
				if (!java->ExceptionCheck()) {
					jobject wrapperExceptionObject = java->NewObject(wrapperClazz, wrapperConstructor, messageText, cause);
					if (!java->ExceptionCheck()) {
						return static_cast<jthrowable>(wrapperExceptionObject);
					}
				}
			} catch (...) {
				// This should never fail. Just return a NULL pointer, the caller will handle it.
			}
		}

		return 0;
	}

	const char * constructNativeString(JNIEnv * java, const jstring javaString) {
		return (javaString != 0) ? java->GetStringUTFChars(javaString, 0) : 0;
	}

	void releaseNativeString(JNIEnv * java, const jstring javaString, const char * nativeString) {
		if (nativeString != 0) {
			java->ReleaseStringUTFChars(javaString, nativeString);
		}
	}

}

JNIEXPORT jobject JNICALL Java_myscubadiary_main_java_core_DeviceConnector_getSupportedDevices(JNIEnv * java, jclass clazz) {
	const jclass nativeExceptionClass = java->FindClass("myscubadiary/main/java/core/exceptions/NativeException");
	const jclass containerClass = java->FindClass("java/util/TreeSet");
	const jclass deviceClass = java->FindClass("myscubadiary/main/java/core/Device");
	const jclass interfaceClass = java->FindClass("myscubadiary/main/java/core/Device$Interface");

	if ((nativeExceptionClass == 0) || (containerClass == 0) || (deviceClass == 0) || (interfaceClass == 0)) {
		return throwExceptionMessage(java, nativeExceptionClass, "Could not find the necessary Java classes.");
	}

	const jmethodID nativeExceptionConstructor = java->GetMethodID(nativeExceptionClass, "<init>", "(Ljava/lang/String;Ljava/lang/Throwable;)V");
	const jmethodID containerConstructor = java->GetMethodID(containerClass, "<init>", "()V");
	const jmethodID containerAdd = java->GetMethodID(containerClass, "add", "(Ljava/lang/Object;)Z");
	const jmethodID deviceConstructor = java->GetMethodID(deviceClass, "<init>", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lmyscubadiary/main/java/core/Device$Interface;)V");
	const jmethodID interfaceValueOf = java->GetStaticMethodID(interfaceClass, "valueOf", "(I)Lmyscubadiary/main/java/core/Device$Interface;");

	if ((nativeExceptionConstructor == 0) || (containerConstructor == 0) || (containerAdd == 0) || (deviceConstructor == 0) || (interfaceValueOf == 0)) {
		return throwExceptionMessage(java, nativeExceptionClass, "Could not find the necessary Java methods.");
	}

	jobject container = java->NewObject(containerClass, containerConstructor);

	if (java->ExceptionCheck()) {
		return handleJavaException(java, nativeExceptionClass, nativeExceptionConstructor, "Could not create container.");
	}

	try {
		const vector<device_info_t> supportedNames(AutoConnector::listSupportedDevices());

		const vector<device_info_t>::const_iterator listEnd(supportedNames.end());
		for (vector<device_info_t>::const_iterator listItem(supportedNames.begin()); listItem != listEnd; ++listItem) {
			const device_info_t item = *listItem;

			jstring deviceVendor = java->NewStringUTF(item.vendorName.c_str());
			jstring deviceModel = java->NewStringUTF(item.productName.c_str());
			jobject deviceInterface = java->CallStaticObjectMethod(interfaceClass, interfaceValueOf, static_cast<int>(item.communicationInterface));

			if (java->ExceptionCheck()) {
				return handleJavaException(java, nativeExceptionClass, nativeExceptionConstructor, "Unknown device communication interface encountered.");
			}

			jobject device = java->NewObject(deviceClass, deviceConstructor, deviceVendor, deviceModel, deviceModel, deviceInterface);

			if (java->ExceptionCheck()) {
				return handleJavaException(java, nativeExceptionClass, nativeExceptionConstructor, "Could not create device.");
			}

			java->CallBooleanMethod(container, containerAdd, device);

			if (java->ExceptionCheck()) {
				return handleJavaException(java, nativeExceptionClass, nativeExceptionConstructor, "Could not save device.");
			}
		}

		return container;

	} catch (const exception & e) {
		return throwExceptionMessage(java, nativeExceptionClass, e.what());
	} catch (...) {
		return throwExceptionMessage(java, nativeExceptionClass, "Unexpected native exception.");
	}
}

JNIEXPORT jobject JNICALL Java_myscubadiary_main_java_core_DeviceConnector_loadUserDivesFromDevice(JNIEnv * java, jclass clazz, jstring devicePort, jstring deviceVendor, jstring deviceModel, jobject container) {
	const jclass containerClass = java->FindClass("java/util/Set");
	const jclass nativeExceptionClass = java->FindClass("myscubadiary/main/java/core/exceptions/NativeException");
	const jclass communicationExceptionClass = java->FindClass("myscubadiary/main/java/core/exceptions/CommunicationException");
	const jclass unknownDeviceExceptionClass = java->FindClass("myscubadiary/main/java/core/exceptions/UnknownDeviceException");
	const jclass deviceClass = java->FindClass("myscubadiary/main/java/core/Device");
	const jclass gasClass = java->FindClass("myscubadiary/main/java/core/Gas");
	const jclass diveClass = java->FindClass("myscubadiary/main/java/core/Dive");
	const jclass cylinderClass = java->FindClass("myscubadiary/main/java/core/Cylinder");
	const jclass integerClass = java->FindClass("java/lang/Integer");
	const jclass floatClass = java->FindClass("java/lang/Float");

	if ((containerClass == 0) || (nativeExceptionClass == 0) || (communicationExceptionClass == 0) || (unknownDeviceExceptionClass == 0) || (deviceClass == 0) || (gasClass == 0) || (diveClass == 0) || (cylinderClass == 0) || (integerClass == 0) || (floatClass == 0)) {
		return throwExceptionMessage(java, nativeExceptionClass, "Could not find the necessary Java classes.");
	}

	const jmethodID containerAdd = java->GetMethodID(containerClass, "add", "(Ljava/lang/Object;)Z");
	const jmethodID nativeExceptionConstructor = java->GetMethodID(nativeExceptionClass, "<init>", "(Ljava/lang/String;Ljava/lang/Throwable;)V");
	const jmethodID deviceConstructor = java->GetMethodID(deviceClass, "<init>", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
	const jmethodID gasBlendGas = java->GetStaticMethodID(gasClass, "blendGas", "(II)Lmyscubadiary/main/java/core/Gas;");
	const jmethodID diveConstructor = java->GetMethodID(diveClass, "<init>", "(Lmyscubadiary/main/java/core/Device;Ljava/lang/String;FFLjava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Lmyscubadiary/main/java/core/Gas;Lmyscubadiary/main/java/core/Cylinder;)V");
	const jmethodID diveSetDiveProfileAndUpdate = java->GetMethodID(diveClass, "setDiveProfileAndUpdate", "([Ljava/lang/Float;[Ljava/lang/Float;[Ljava/lang/Float;[Ljava/lang/Float;[Ljava/lang/Integer;)V");
	const jmethodID cylinderConstructor = java->GetMethodID(cylinderClass, "<init>", "(FF)V");
	const jmethodID integerConstructor = java->GetMethodID(integerClass, "<init>", "(I)V");
	const jmethodID floatConstructor = java->GetMethodID(floatClass, "<init>", "(F)V");

	if ((containerAdd == 0) || (nativeExceptionConstructor == 0) || (deviceConstructor == 0) || (gasBlendGas == 0) || (diveConstructor == 0) || (diveSetDiveProfileAndUpdate == 0) || (cylinderConstructor == 0) || (integerConstructor == 0) || (floatConstructor == 0)) {
		return throwExceptionMessage(java, nativeExceptionClass, "Could not find the necessary Java methods.");
	}

	const char * devicePortNative = constructNativeString(java, devicePort);
	const char * deviceVendorNative = constructNativeString(java, deviceVendor);
	const char * deviceModelNative = constructNativeString(java, deviceModel);
	try {
		
		AutoConnector connector(devicePortNative, deviceVendorNative, deviceModelNative);
		AutoDevice autoDevice(connector);
		attachedDevice = &autoDevice;

		const auto_ptr<dive_data_t> dives(autoDevice.downloadDives());
		releaseNativeString(java, deviceModel, deviceModelNative);
		releaseNativeString(java, deviceVendor, deviceVendorNative);
		releaseNativeString(java, devicePort, devicePortNative);

		jobject device = java->NewObject(deviceClass, deviceConstructor, deviceVendor, deviceModel, java->NewStringUTF(autoDevice.getSerial()));

		if (java->ExceptionCheck()) {
			return handleJavaException(java, nativeExceptionClass, nativeExceptionConstructor, "Could not create parent device.");
		}

		const vector<Dive>::const_iterator logbookEnd(dives->logbook.end());
		for (vector<Dive>::const_iterator logbookItem(dives->logbook.begin()); logbookItem != logbookEnd; ++logbookItem) {
			const Dive & item(*logbookItem);

			const dc_gasmix_t * const primaryGasMix = item.getPrimaryGasMix();
			jobject gas = 0;
			if (primaryGasMix != 0) {
				gas = java->CallStaticObjectMethod(gasClass, gasBlendGas, roundToInt(primaryGasMix->oxygen * 100.0), roundToInt(primaryGasMix->helium * 100.0));
			
				if (java->ExceptionCheck()) {
					return handleJavaException(java, nativeExceptionClass, nativeExceptionConstructor, "Could not create gas.");
				}
			}
			const double primaryMixUsedPressure = item.getUsedPressure();
			jobject usedPressure = 0;
			if (primaryMixUsedPressure > 0) {
				usedPressure = java->NewObject(floatClass, floatConstructor, primaryMixUsedPressure);

				if (java->ExceptionCheck()) {
					return handleJavaException(java, nativeExceptionClass, nativeExceptionConstructor, "Could not create used pressure.");
				}
			}
			const double * const diveBottomTemperature = item.getBottomTemperature();
			jobject bottomTemperature = 0;
			if (diveBottomTemperature != 0) {
				bottomTemperature = java->NewObject(floatClass, floatConstructor, *diveBottomTemperature);

				if (java->ExceptionCheck()) {
					return handleJavaException(java, nativeExceptionClass, nativeExceptionConstructor, "Could not create bottom temperature.");
				}
			}
			const dc_tank_t * const primaryTank = item.getPrimaryGasTank();
			jobject cylinder = 0;
			if (primaryTank != 0) {
				cylinder = java->NewObject(cylinderClass, cylinderConstructor, primaryTank->volume, primaryTank->workpressure);
			
				if (java->ExceptionCheck()) {
					return handleJavaException(java, nativeExceptionClass, nativeExceptionConstructor, "Could not create cylinder.");
				}
			}
			const double avgDepth = item.getAvgDepth();
			jobject averageDepth = 0;
			if (avgDepth > 0) {
				averageDepth = java->NewObject(floatClass, floatConstructor, avgDepth);

				if (java->ExceptionCheck()) {
					return handleJavaException(java, nativeExceptionClass, nativeExceptionConstructor, "Could not create used pressure.");
				}
			}

			char timestamp[20];
			jobject dive = java->NewObject(diveClass, diveConstructor, device, java->NewStringUTF(dateTimeToTimestamp(item.dateTime, timestamp)), item.diveTime, item.maxDepth, averageDepth, usedPressure, bottomTemperature, gas, cylinder);

			if (java->ExceptionCheck()) {
				return handleJavaException(java, nativeExceptionClass, nativeExceptionConstructor, "Could not create dive.");
			}

			/*
			 * Some devices may generate intermediate profile records between
			 * standard sampling intervals to store indeterministic events such as bearing change.
			 * These intermediate samples may not contain depth value.
			 * Filter out any profile points that do not include depth readings.
			 */
			const set<unsigned int> profileIndexesWithNullDepthValue(findIndexesWithNullValue(item.profile.depths));
			const DiveProfile filteredProfile(item.profile.filter(profileIndexesWithNullDepthValue));

			/*
			 * Remove any negative values (introduced by machine precision limitations) of
			 * intrinsically positive quantities by replacing them with 0.0.
			 */
			const vector<shared_ptr<float>>positiveProfileTimes(replaceNegativeWithZero(filteredProfile.times));
			const vector<shared_ptr<float>>positiveProfileDepths(replaceNegativeWithZero(filteredProfile.depths));
			const vector<shared_ptr<float>>positiveProfilePressures(replaceNegativeWithZero(filteredProfile.pressures));

			const jsize profileLength = filteredProfile.size;
			if (profileLength > 0) {
				jobjectArray times = java->NewObjectArray(profileLength, floatClass, 0);
				jobjectArray depths = java->NewObjectArray(profileLength, floatClass, 0);
				jobjectArray pressures = java->NewObjectArray(profileLength, floatClass, 0);
				jobjectArray temperatures = java->NewObjectArray(profileLength, floatClass, 0);
				jobjectArray events = java->NewObjectArray(profileLength, integerClass, 0);

				for (jsize index = 0; index < profileLength; ++index) {
					if (setObjectArrayElement(java, times, index, floatClass, floatConstructor, positiveProfileTimes[index]) ||
						setObjectArrayElement(java, depths, index, floatClass, floatConstructor, positiveProfileDepths[index]) ||
						setObjectArrayElement(java, pressures, index, floatClass, floatConstructor, positiveProfilePressures[index]) ||
						setObjectArrayElement(java, temperatures, index, floatClass, floatConstructor, filteredProfile.temperatures[index]) ||
						setObjectArrayElement(java, events, index, integerClass, integerConstructor, filteredProfile.events[index])) {
							return handleJavaException(java, nativeExceptionClass, nativeExceptionConstructor, "Error while converting profile data.");
					}
				}

				java->CallVoidMethod(dive, diveSetDiveProfileAndUpdate, times, depths, pressures, temperatures, events);

				if (java->ExceptionCheck()) {
					return handleJavaException(java, nativeExceptionClass, nativeExceptionConstructor, "Could not set dive profile.");
				}
			}

			java->CallBooleanMethod(container, containerAdd, dive);
			
			if (java->ExceptionCheck()) {
				return handleJavaException(java, nativeExceptionClass, nativeExceptionConstructor, "Could not save dive.");
			}
		}

		return device;
	} catch (const UnknownDeviceException & e) {
		releaseNativeString(java, deviceModel, deviceModelNative);
		releaseNativeString(java, deviceVendor, deviceVendorNative);
		releaseNativeString(java, devicePort, devicePortNative);
		return throwExceptionMessage(java, unknownDeviceExceptionClass, e.what());
	} catch (const CommunicationException & e) {
		releaseNativeString(java, deviceModel, deviceModelNative);
		releaseNativeString(java, deviceVendor, deviceVendorNative);
		releaseNativeString(java, devicePort, devicePortNative);
		return throwExceptionMessage(java, communicationExceptionClass, e.what());
	} catch (const exception & e) {
		releaseNativeString(java, deviceModel, deviceModelNative);
		releaseNativeString(java, deviceVendor, deviceVendorNative);
		releaseNativeString(java, devicePort, devicePortNative);
		return throwExceptionMessage(java, nativeExceptionClass, e.what());
	} catch (...) {
		releaseNativeString(java, deviceModel, deviceModelNative);
		releaseNativeString(java, deviceVendor, deviceVendorNative);
		releaseNativeString(java, devicePort, devicePortNative);
		return throwExceptionMessage(java, nativeExceptionClass, "Unexpected native exception.");
	}
}

JNIEXPORT void JNICALL Java_myscubadiary_main_java_core_DeviceConnector_interruptDeviceDownload(JNIEnv * java, jclass clazz) {
	attachedDevice->interrupt();
}
