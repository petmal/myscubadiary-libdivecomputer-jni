/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#include "UnknownDeviceException.h"

using namespace std;

UnknownDeviceException::UnknownDeviceException(const char * devicePort, const char * vendorName, const char * deviceName)
	: Exception(string("No matching device found: ")
	.append("vendor='").append(vendorName != 0 ? vendorName : "")
	.append("', model='").append(deviceName != 0 ? deviceName : "")
	.append("', port='").append(devicePort != 0 ? devicePort : "")
	.append("'")) {}
