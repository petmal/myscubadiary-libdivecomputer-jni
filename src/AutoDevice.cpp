/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#include "AutoDevice.h"

using namespace std;

bool AutoDevice::wasInterrupted = false;
const char * AutoDevice::g_cachedir = NULL;
int AutoDevice::g_cachedir_read = 1;

int AutoDevice::cancel_cb(void * userdata)
{
	return Utility::g_cancel;
}

void AutoDevice::event_cb(dc_device_t * device, dc_event_type_t event, const void * data, void * userdata)
{
	const dc_event_progress_t *progress = (dc_event_progress_t *) data;
	const dc_event_devinfo_t *devinfo = (dc_event_devinfo_t *) data;
	const dc_event_clock_t *clock = (dc_event_clock_t *) data;

	device_data_t *devdata = static_cast<device_data_t *>(userdata);

	switch (event) {
	case DC_EVENT_WAITING:
		cout << "Event: waiting for user action" << endl;
		break;
	case DC_EVENT_PROGRESS:
		printf ("Event: progress %3.2f%% (%u/%u)\n",
			100.0 * (double) progress->current / (double) progress->maximum,
			progress->current, progress->maximum);
		break;
	case DC_EVENT_DEVINFO:
		devdata->devinfo = *devinfo;
		printf ("Event: model=%u (0x%08x), firmware=%u (0x%08x), serial=%u (0x%08x)\n",
			devinfo->model, devinfo->model,
			devinfo->firmware, devinfo->firmware,
			devinfo->serial, devinfo->serial);
		if (g_cachedir && g_cachedir_read) {
			dc_buffer_t *fingerprint = Utility::fpread(g_cachedir, dc_device_get_type (device), devinfo->serial);
			dc_device_set_fingerprint (device,
				dc_buffer_get_data (fingerprint),
				dc_buffer_get_size (fingerprint));
			dc_buffer_free (fingerprint);
		}
		break;
	case DC_EVENT_CLOCK:
		devdata->clock = *clock;
		printf ("Event: systime=" DC_TICKS_FORMAT ", devtime=%u\n",
			clock->systime, clock->devtime);
		break;
	default:
		break;
	}
}

int AutoDevice::dive_cb(const unsigned char * data, unsigned int size, const unsigned char * fingerprint, unsigned int fsize, void * userdata)
{
	if (!wasInterrupted) {
		dive_data_t & divedata = *(static_cast<dive_data_t *>(userdata));
		divedata.logbook.push_back(Dive(AutoParser(divedata.device, data, size)));
		return 1;
	}
	return 0;
}

AutoDevice::AutoDevice(const AutoConnector & connector) : device(0), deviceDescriptor(connector.descriptor) {
	AutoDevice::wasInterrupted = false;
	open(connector.context, connector.descriptor, connector.port);
	registerEventHandler();
	registerCancelHandler();
	registerFingerprintData(connector.fp);
	strcpy(serial, "Unknown");
}
AutoDevice::~AutoDevice() {
	const dc_status_t status = dc_device_close(device);
	if (status != DC_STATUS_SUCCESS) {
		cout << "Error closing the device (" << status << ")." << endl;
	}
}
// Open.
void AutoDevice::open(dc_context_t * context, dc_descriptor_t * descriptor, const char * devname) {
	const dc_status_t status = dc_device_open(&device, context, descriptor, devname);
	if (status != DC_STATUS_SUCCESS) {
		throw CommunicationException("Error opening device.", status);
	}
}
// Register event handler.
void AutoDevice::registerEventHandler() {
	int events = DC_EVENT_WAITING | DC_EVENT_PROGRESS | DC_EVENT_DEVINFO | DC_EVENT_CLOCK;
	const dc_status_t status = dc_device_set_events(device, events, event_cb, &deviceData);
	if (status != DC_STATUS_SUCCESS) {
		throw Exception("Error registering the event handler.", status);
	}
}
	// Register the cancellation handler.
void AutoDevice::registerCancelHandler() {
	const dc_status_t status = dc_device_set_cancel(device, cancel_cb, NULL);
	if (status != DC_STATUS_SUCCESS) {
		throw Exception("Error registering the cancellation handler.", status);
	}
}
	// Register the fingerprint data.
void AutoDevice::registerFingerprintData(dc_buffer_t * fingerprint) {
	if (fingerprint) {
		const dc_status_t status = dc_device_set_fingerprint(device, dc_buffer_get_data (fingerprint), dc_buffer_get_size (fingerprint));
		if (status != DC_STATUS_SUCCESS) {
			throw Exception("Error registering the fingerprint data.", status);
		}
	}
}
// Download dives.
auto_ptr<dive_data_t> AutoDevice::downloadDives() {
	auto_ptr<dive_data_t> divedata(new dive_data_t(device));

	const dc_status_t status = dc_device_foreach(device, dive_cb, divedata.get());
	if (status != DC_STATUS_SUCCESS) {
		throw CommunicationException("Error downloading the dives.", status);
	}

	sprintf(serial, "%u", deviceData.devinfo.serial);

	return divedata;
}
const char * AutoDevice::getSerial() const {
	return serial;
}

void AutoDevice::interrupt() {
	AutoDevice::wasInterrupted = true;
}