/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#include "dive_data_t.h"

using namespace std;

dive_data_t::dive_data_t(dc_device_t * device)
	: device(device), number(0), fingerprint(0) {}

dive_data_t::~dive_data_t() {
	dc_buffer_free(fingerprint);
}