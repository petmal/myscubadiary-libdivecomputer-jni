/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#include "AutoParser.h"

using namespace std;

void AutoParser::sample_cb(dc_sample_type_t type, dc_sample_value_t value, void * userdata)
{
	DiveProfile & profile = *(static_cast<DiveProfile *>(userdata));

	switch (type) {
	case DC_SAMPLE_TIME:
		if (profile.size++) {
			profile.balance(profile.size - 1);
		}
		profile.times.push_back(make_shared<float>(value.time));
		break;
	case DC_SAMPLE_DEPTH:
		profile.depths.push_back(make_shared<float>(value.depth));
		break;
	case DC_SAMPLE_PRESSURE:
		profile.pressures.push_back(make_shared<float>(value.pressure.value));
		break;
	case DC_SAMPLE_TEMPERATURE:
		profile.temperatures.push_back(make_shared<float>(value.temperature));
		break;
	case DC_SAMPLE_EVENT:
		profile.events.push_back(make_shared<long>(value.event.type));
		break;
	case DC_SAMPLE_RBT:
		
		break;
	case DC_SAMPLE_HEARTBEAT:
		
		break;
	case DC_SAMPLE_BEARING:
		
		break;
	case DC_SAMPLE_VENDOR:
		
		break;
	default:
		break;
	}
}

AutoParser::AutoParser(dc_device_t * device, const unsigned char data[], unsigned int size) : parser(0) {
	parserInit(device);
	registerParserData(data, size);
}
AutoParser::~AutoParser() {
	const dc_status_t status = dc_parser_destroy(parser);
	if (status != DC_STATUS_SUCCESS) {
		cout << "Error destroying the parser (" << status << ")." << endl;
	}
}
// Parser init.
void AutoParser::parserInit(dc_device_t * device) {
	const dc_status_t status = dc_parser_new(&parser, device);
	if (status != DC_STATUS_SUCCESS) {
		throw Exception("Error creating the parser.", status);
	}
}
// Register the data.
void AutoParser::registerParserData(const unsigned char data[], unsigned int size) {
	const dc_status_t status = dc_parser_set_data(parser, data, size);
	if (status != DC_STATUS_SUCCESS) {
		throw Exception("Error registering the data.", status);
	}
}
// Parse the datetime.
dc_datetime_t AutoParser::getDateTime() const {
	dc_datetime_t dateTime = {0};
	const dc_status_t status = dc_parser_get_datetime(parser, &dateTime);
	if (status != DC_STATUS_SUCCESS && status != DC_STATUS_UNSUPPORTED) {
		throw Exception("Error parsing the datetime.", status);
	}

	return dateTime;
}
// Parse the divetime.
unsigned int AutoParser::getDiveTime() const {
	unsigned int diveTime = 0;
	const dc_status_t status = dc_parser_get_field(parser, DC_FIELD_DIVETIME, 0, &diveTime);
	if (status != DC_STATUS_SUCCESS && status != DC_STATUS_UNSUPPORTED) {
		throw Exception("Error parsing the divetime.", status);
	}

	return diveTime;
}
// Parse the maxdepth.
double AutoParser::getMaxDepth() const {
	double maxDepth = 0.0;
	const dc_status_t status = dc_parser_get_field(parser, DC_FIELD_MAXDEPTH, 0, &maxDepth);
	if (status != DC_STATUS_SUCCESS && status != DC_STATUS_UNSUPPORTED) {
		throw Exception("Error parsing the maxdepth.", status);
	}

	return maxDepth;
}
// Parse the avgdepth.
shared_ptr<double> AutoParser::getAvgDepth() const {
	double avgDepth = 0;
	const dc_status_t status = dc_parser_get_field(parser, DC_FIELD_AVGDEPTH, 0, &avgDepth);
	if (status == DC_STATUS_SUCCESS) {
		return make_shared<double>(avgDepth);
	} else if (status != DC_STATUS_UNSUPPORTED) {
		throw Exception("Error parsing the avgdepth.", status);
	}

	return 0;
}
// Parse the bottom temperature.
shared_ptr<double> AutoParser::getBottomTemperature() const {
	double temperature = 0;
	const dc_status_t status = dc_parser_get_field(parser, DC_FIELD_TEMPERATURE_MINIMUM, 0, &temperature);
	if (status == DC_STATUS_SUCCESS) {
		return make_shared<double>(temperature);
	} else if (status != DC_STATUS_UNSUPPORTED) {
		throw Exception("Error parsing the temperature.", status);
	}

	return 0;
}
// Parse the gas mixes.
vector<dc_gasmix_t> AutoParser::getGasMixes() const {
	vector<dc_gasmix_t> mixes;
    const unsigned int numGases = getNumGases();
    for (unsigned int i = 0; i < numGases; ++i) {
		dc_gasmix_t gas = {0};
        const dc_status_t status = dc_parser_get_field(parser, DC_FIELD_GASMIX, i, &gas);
        if (status == DC_STATUS_SUCCESS) {
			mixes.push_back(gas);
		} else if (status != DC_STATUS_UNSUPPORTED) {
			throw Exception("Error parsing the gas mix.", status);
		}
	}

	return mixes;
}
// Parse number of gases.
unsigned int AutoParser::getNumGases() const {
       unsigned int ngases = 0;
       const dc_status_t status = dc_parser_get_field(parser, DC_FIELD_GASMIX_COUNT, 0, &ngases);
	   if (status != DC_STATUS_SUCCESS && status != DC_STATUS_UNSUPPORTED) {
		   throw Exception("Error parsing the gas mix count.", status);
	   }

	return ngases;
}
// Parse the tanks.
vector<dc_tank_t> AutoParser::getTanks() const {
	vector<dc_tank_t> tanks;
    const unsigned int numTanks = getNumTanks();
    for (unsigned int i = 0; i < numTanks; ++i) {
		dc_tank_t tank = {0};
        const dc_status_t status = dc_parser_get_field(parser, DC_FIELD_TANK, i, &tank);
        if (status == DC_STATUS_SUCCESS) {
			tanks.push_back(tank);
		} else if (status != DC_STATUS_UNSUPPORTED) {
			throw Exception("Error parsing the tank.", status);
		}
	}

	return tanks;
}
// Parse number of tanks.
unsigned int AutoParser::getNumTanks() const {
	unsigned int ntanks = 0;
	const dc_status_t status = dc_parser_get_field(parser, DC_FIELD_TANK_COUNT, 0, &ntanks);
	if (status != DC_STATUS_SUCCESS && status != DC_STATUS_UNSUPPORTED) {
		throw Exception("Error parsing the tank count.", status);
	}

	return ntanks;
}
// Parse the sample data.
DiveProfile AutoParser::getDiveProfile() const {
	DiveProfile profile;

	const dc_status_t status = dc_parser_samples_foreach(parser, AutoParser::sample_cb, &profile);
	if (status != DC_STATUS_SUCCESS) {
		throw Exception("Error parsing the sample data.", status);
	}

	if (profile.size > 0) { profile.balance(profile.size); }

	return profile;
}
