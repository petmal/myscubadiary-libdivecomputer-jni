/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#include "Utility.h"

using namespace std;

void Utility::sighandler(int signum)
{
#ifndef _WIN32
	// Restore the default signal handler.
	signal (signum, SIG_DFL);
#endif

	g_cancel = 1;
}

const string Utility::build_full_device_name(const char * vendorName, const char * deviceName)
{
	string full_name("");
	if (vendorName != 0) {
		full_name.append(vendorName);
		full_name.append(" ");
	}
	full_name.append(deviceName);
	return static_cast<const string>(full_name);
}

dc_status_t Utility::search(dc_descriptor_t **out, const char *name)
{
	dc_iterator_t *iterator = NULL;
	dc_status_t rc = dc_descriptor_iterator (&iterator);
	if (rc != DC_STATUS_SUCCESS) {
		cout << "Error creating the device descriptor iterator." << endl;
		return rc;
	}

	dc_descriptor_t *descriptor = NULL, *current = NULL;
	while ((rc = dc_iterator_next (iterator, &descriptor)) == DC_STATUS_SUCCESS) {
		if (name) {
			const char *vendor = dc_descriptor_get_vendor (descriptor);
			const char *product = dc_descriptor_get_product (descriptor);

			size_t n = strlen (vendor);
			if (strncasecmp (name, vendor, n) == 0 && name[n] == ' ' &&
				strcasecmp (name + n + 1, product) == 0)
			{
				current = descriptor;
				break;
			} else if (strcasecmp (name, product) == 0) {
				current = descriptor;
				break;
			}
		}

		dc_descriptor_free (descriptor);
	}

	if (rc != DC_STATUS_SUCCESS && rc != DC_STATUS_DONE) {
		dc_descriptor_free (current);
		dc_iterator_free (iterator);
		cout << "Error iterating the device descriptors." << endl;
		return rc;
	}

	dc_iterator_free (iterator);

	*out = current;

	return DC_STATUS_SUCCESS;
}

dc_family_t Utility::lookup_type(const char * vendorName, const char * deviceName)
{
	/*unsigned int nbackends = sizeof (g_backends) / sizeof (g_backends[0]);
	for (unsigned int i = 0; i < nbackends; ++i) {
		if (strcmp (name, g_backends[i].name) == 0)
			return g_backends[i].type;
	}

	return DC_FAMILY_NULL;*/

	dc_family_t backend = DC_FAMILY_NULL;

	dc_iterator_t * iterator = 0;
	dc_descriptor_t * descriptor = 0;
	dc_descriptor_iterator(&iterator);
	while (dc_iterator_next(iterator, &descriptor) == DC_STATUS_SUCCESS) {
		if ((strcmp(vendorName, dc_descriptor_get_vendor(descriptor)) == 0) && (strcmp(deviceName, dc_descriptor_get_product(descriptor)) == 0)) {
			backend = dc_descriptor_get_type(descriptor);
			dc_descriptor_free(descriptor);
			break;
		}
		dc_descriptor_free(descriptor);
	}
	dc_iterator_free(iterator);

	return backend;
}

const char * Utility::lookup_name(dc_family_t type)
{
	static const backend_table_t g_backends[] = {
	{"solution",    DC_FAMILY_SUUNTO_SOLUTION},
	{"eon",	        DC_FAMILY_SUUNTO_EON},
	{"vyper",       DC_FAMILY_SUUNTO_VYPER},
	{"vyper2",      DC_FAMILY_SUUNTO_VYPER2},
	{"d9",          DC_FAMILY_SUUNTO_D9},
	{"aladin",      DC_FAMILY_UWATEC_ALADIN},
	{"memomouse",   DC_FAMILY_UWATEC_MEMOMOUSE},
	{"smart",       DC_FAMILY_UWATEC_SMART},
	{"sensus",      DC_FAMILY_REEFNET_SENSUS},
	{"sensuspro",   DC_FAMILY_REEFNET_SENSUSPRO},
	{"sensusultra", DC_FAMILY_REEFNET_SENSUSULTRA},
	{"vtpro",       DC_FAMILY_OCEANIC_VTPRO},
	{"veo250",      DC_FAMILY_OCEANIC_VEO250},
	{"atom2",       DC_FAMILY_OCEANIC_ATOM2},
	{"nemo",        DC_FAMILY_MARES_NEMO},
	{"puck",        DC_FAMILY_MARES_PUCK},
	{"darwin",      DC_FAMILY_MARES_DARWIN},
	{"iconhd",      DC_FAMILY_MARES_ICONHD},
	{"ostc",        DC_FAMILY_HW_OSTC},
	{"frog",        DC_FAMILY_HW_FROG},
	{"ostc3",       DC_FAMILY_HW_OSTC3},
	{"edy",         DC_FAMILY_CRESSI_EDY},
	{"leonardo",	DC_FAMILY_CRESSI_LEONARDO},
	{"n2ition3",    DC_FAMILY_ZEAGLE_N2ITION3},
	{"cobalt",      DC_FAMILY_ATOMICS_COBALT},
	{"predator",	DC_FAMILY_SHEARWATER_PREDATOR},
	{"petrel",      DC_FAMILY_SHEARWATER_PETREL}
	};

	unsigned int nbackends = sizeof (g_backends) / sizeof (g_backends[0]);
	for (unsigned int i = 0; i < nbackends; ++i) {
		if (g_backends[i].type == type)
			return g_backends[i].name;
	}

	return NULL;
}

unsigned char Utility::hex2dec(unsigned char value)
{
	if (value >= '0' && value <= '9')
		return value - '0';
	else if (value >= 'A' && value <= 'F')
		return value - 'A' + 10;
	else if (value >= 'a' && value <= 'f')
		return value - 'a' + 10;
	else
		return 0;
}

dc_buffer_t * Utility::fpconvert(const char *fingerprint)
{
	// Get the length of the fingerprint data.
	size_t nbytes = (fingerprint ? strlen (fingerprint) / 2 : 0);
	if (nbytes == 0)
		return NULL;

	// Allocate a memory buffer.
	dc_buffer_t *buffer = dc_buffer_new (nbytes);

	// Convert the hexadecimal string.
	for (unsigned int i = 0; i < nbytes; ++i) {
		unsigned char msn = hex2dec (fingerprint[i * 2 + 0]);
		unsigned char lsn = hex2dec (fingerprint[i * 2 + 1]);
		unsigned char byte = (msn << 4) + lsn;

		dc_buffer_append (buffer, &byte, 1);
	}

	return buffer;
}

dc_buffer_t * Utility::fpread(const char *dirname, dc_family_t backend, unsigned int serial)
{
	// Build the filename.
	char filename[1024] = {0};
	snprintf (filename, sizeof (filename), "%s/%s-%08X.bin",
		dirname, lookup_name (backend), serial);

	// Open the fingerprint file.
	FILE *fp = fopen (filename, "rb");
	if (fp == NULL)
		return NULL;

	// Allocate a memory buffer.
	dc_buffer_t *buffer = dc_buffer_new (0);

	// Read the entire file into the buffer.
	size_t n = 0;
	unsigned char block[1024] = {0};
	while ((n = fread (block, 1, sizeof (block), fp)) > 0) {
		dc_buffer_append (buffer, block, n);
	}

	// Close the file.
	fclose (fp);

	return buffer;
}

void Utility::fpwrite(dc_buffer_t *buffer, const char *dirname, dc_family_t backend, unsigned int serial)
{
	// Check the buffer size.
	if (dc_buffer_get_size (buffer) == 0)
		return;

	// Build the filename.
	char filename[1024] = {0};
	snprintf (filename, sizeof (filename), "%s/%s-%08X.bin",
		dirname, lookup_name (backend), serial);

	// Open the fingerprint file.
	FILE *fp = fopen (filename, "wb");
	if (fp == NULL)
		return;

	// Write the fingerprint data.
	fwrite (dc_buffer_get_data (buffer), 1, dc_buffer_get_size (buffer), fp);

	// Close the file.
	fclose (fp);
}