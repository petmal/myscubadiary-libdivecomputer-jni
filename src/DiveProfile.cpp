/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#include "DiveProfile.h"

using namespace std;

DiveProfile::DiveProfile() : size(0) {}

void DiveProfile::balance(const unsigned int targetSize) {
	padVectorToSize(times, targetSize);
	padVectorToSize(depths, targetSize);
	padVectorToSize(pressures, targetSize);
	padVectorToSize(temperatures, targetSize);
	padVectorToSize(events, targetSize);
}

DiveProfile DiveProfile::filter(const set<unsigned int> & filteredIndexes) const {
	if (!filteredIndexes.empty()) {
		DiveProfile filtered;
		for (unsigned int index = 0; index < size; ++index) {
			if (filteredIndexes.count(index) == 0) {
				filtered.times.push_back(times[index]);
				filtered.depths.push_back(depths[index]);
				filtered.pressures.push_back(pressures[index]);
				filtered.temperatures.push_back(temperatures[index]);
				filtered.events.push_back(events[index]);
			}
		}
		filtered.size = filtered.times.size();

		return filtered;
	}

	return DiveProfile(*this);
}