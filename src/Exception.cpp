/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#include "Exception.h"

using namespace std;

Exception::Exception(const string & message) : message(message) {}

Exception::Exception(const string & message, const int errorCode) : message("[" + to_string(static_cast<long long>(errorCode)) + "]: " + message) {}

const char * Exception::what() const throw() {
	return message.c_str();
}