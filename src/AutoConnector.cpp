/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#include "AutoConnector.h"

using namespace std;

vector<device_info_t> AutoConnector::listSupportedDevices() {
	dc_iterator_t * iterator = NULL;
	dc_descriptor_t * descriptor = NULL;
	dc_descriptor_iterator(&iterator);
	vector<device_info_t> deviceList;
	while (dc_iterator_next(iterator, &descriptor) == DC_STATUS_SUCCESS) {
		device_info_t device_info;
		device_info.vendorName = dc_descriptor_get_vendor(descriptor);
		device_info.productName = dc_descriptor_get_product(descriptor);
		device_info.communicationInterface = dc_descriptor_get_transport(descriptor);

		deviceList.push_back(device_info);
		dc_descriptor_free(descriptor);
	}
	dc_iterator_free(iterator);

	return deviceList;
}

AutoConnector::AutoConnector(const char * devicePort, const char * vendorName, const char * deviceName)
	: name(Utility::build_full_device_name(vendorName, deviceName)),
	fingerprint(0),
	port(devicePort),
	context(0),
	descriptor(0),
	fp(0){
		signal(SIGINT, Utility::sighandler);
		dc_status_t status = dc_context_new(&context);
		if (status != DC_STATUS_SUCCESS) { throw Exception("Could not create the context.", status); }
		status = Utility::search(&descriptor, name.c_str());
		if (status != DC_STATUS_SUCCESS) { throw Exception("No matching device descriptor found.", status); }
		if (descriptor == 0) { throw UnknownDeviceException(devicePort, vendorName, deviceName); }
		fp = Utility::fpconvert(fingerprint);
}
AutoConnector::~AutoConnector() {
	dc_buffer_free(fp);
	dc_descriptor_free(descriptor);
	dc_context_free(context);
}
