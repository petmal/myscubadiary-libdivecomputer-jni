# About #

This is an auxiliary repository hosting the Java Native Interface (JNI) library used by 'My SCUBA Diary' to interface with 'libdivecomputer'.

* My SCUBA Diary is a free, lightweight logbook to manage your dives... http://www.myscubadiary.net/
* libdivecomputer is a cross-platform and open source library for communication with dive computers from various manufacturers... http://www.libdivecomputer.org/