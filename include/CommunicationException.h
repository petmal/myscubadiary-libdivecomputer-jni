/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#ifndef COMMUNICATION_EXCEPTION_H_
#define COMMUNICATION_EXCEPTION_H_

#include <string>
#include "Exception.h"

class CommunicationException : public Exception {
public:
	CommunicationException(const std::string & message, const int errorCode);
};

#endif
