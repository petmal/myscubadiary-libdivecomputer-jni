/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#ifndef DIVEPROFILE_H_
#define DIVEPROFILE_H_

#include <vector>
#include <set>
#include <memory>

/**
 * This class represents a dive profile.
 */
class DiveProfile {
private:
	
	template<typename T>
	void padVectorToSize(std::vector<std::shared_ptr<T> > & container, const unsigned int size);

public:

	/** Number of data points in the profile. */
	unsigned int size;

	/** Time samples measured from the beginning of the dive. */
	std::vector<std::shared_ptr<float> > times;

	/** Depth samples. */
	std::vector<std::shared_ptr<float> > depths;

	/** Main cylinder pressure samples. */
	std::vector<std::shared_ptr<float> > pressures;

	/** Water temperature samples. */
	std::vector<std::shared_ptr<float> > temperatures;

	/** Warnings, messages and user triggered events. */
	std::vector<std::shared_ptr<long> > events;

	DiveProfile();

	/**
	 * Pad all data containers to a given size by adding NULL pointers.
	 */
	void balance(const unsigned int targetSize);

	/**
	 * Return a shallow copy of the profile with values at given indexes removed.
	 */
	DiveProfile filter(const std::set<unsigned int> & filteredIndexes) const;
};

	template<typename T>
	void DiveProfile::padVectorToSize(std::vector<std::shared_ptr<T> > & container, const unsigned int size) {
		while (container.size() < size) {
			container.push_back(0);
		}
	}

#endif
