/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#ifndef DIVE_H_
#define DIVE_H_

#include <memory>
#include <libdivecomputer/parser.h>
#include "AutoParser.h"
#include "DiveProfile.h"

/**
 * This structure represents a dive.
 */
class Dive {
private:
	std::vector<dc_gasmix_t> gases;
	std::vector<dc_tank_t> tanks;
	std::shared_ptr<double> avgDepth;
	std::shared_ptr<double> bottomTemperature;
	const dc_tank_t * findPrimaryGasTank() const;

public:

	/** Date and time of the beginning of the dive. */
	const dc_datetime_t dateTime;

	/** Total length of the dive. */
	const double diveTime;

	/** Maximum depth reached during the dive. */
	const double maxDepth;

	/** Dive profile. */
	const DiveProfile profile;

	/**
	 * @param parser Parser to parse this dive from the raw device data.
	 */
	Dive(const AutoParser & parser);

	/** Gas primary gas mix used throughout the dive or NULL if not available (e.g. oxygen <= 0). */
	const dc_gasmix_t * getPrimaryGasMix() const;

	/** Primary gas tank used throughout the dive or NULL if not available (e.g. volume/working pressure <= 0). */
	const dc_tank_t * getPrimaryGasTank() const;

	/** Get pressure used during the dive or 0 if not available (e.g. pressure <= 0). */
	double getUsedPressure() const;

	/** Get average depth or 0 if not available. */
	double getAvgDepth() const;

	/** Get bottom temperature or NULL if not available. */
	const double * getBottomTemperature() const;
};

#endif