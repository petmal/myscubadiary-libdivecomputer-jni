/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#ifndef DEVICE_INFO_T_H_
#define DEVICE_INFO_T_H_

#include <string>
#include <libdivecomputer/descriptor.h>

/**
 * This structure represents type information about a device.
 */
struct device_info_t {
	std::string vendorName;
	std::string productName;
	dc_transport_t communicationInterface;
};

#endif