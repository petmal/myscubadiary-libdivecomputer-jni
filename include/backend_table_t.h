/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#ifndef BACKEND_TABLE_T_H_
#define BACKEND_TABLE_T_H_

/**
 * This structure relates device names to their family types.
 */
struct backend_table_t {
	const char * name;
	dc_family_t type;
};

#endif