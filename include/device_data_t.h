/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#ifndef DEVICE_DATA_T_H_
#define DEVICE_DATA_T_H_

#include <libdivecomputer/device.h>

/**
 * This structure represents technical data about a device.
 */
struct device_data_t {
	dc_event_devinfo_t devinfo;
	dc_event_clock_t clock;
};

#endif