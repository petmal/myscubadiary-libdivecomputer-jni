/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#ifndef UTILITY_H_
#define UTILITY_H_

#include "Define.h"
#include <iostream>
#include <vector>
#include <string>
#include <string.h>

#if defined(_WIN32) || defined(_WIN64)
#include <common.h>
#endif

#include <signal.h>
#include <libdivecomputer/descriptor.h>
#include <libdivecomputer/buffer.h>
#include <libdivecomputer/parser.h>
#include <libdivecomputer/device.h>
#include "backend_table_t.h"
#include "DiveProfile.h"
#include "device_data_t.h"
#include "dive_data_t.h"
#include "Dive.h"
#include "AutoParser.h"


/**
 * Internal utility functions.
 */
namespace Utility {
	static volatile sig_atomic_t g_cancel = 0;

	void sighandler(int signum);
	int cancel_cb(void * userdata);
	const std::string build_full_device_name(const char * vendorName, const char * deviceName);
	dc_status_t search(dc_descriptor_t **out, const char *name);
	dc_family_t lookup_type(const char * vendorName, const char * deviceName);
	const char * lookup_name(dc_family_t type);
	unsigned char hex2dec(unsigned char value);
	dc_buffer_t * fpconvert(const char *fingerprint);
	dc_buffer_t * fpread(const char *dirname, dc_family_t backend, unsigned int serial);
	void fpwrite(dc_buffer_t *buffer, const char *dirname, dc_family_t backend, unsigned int serial);
}

#endif
