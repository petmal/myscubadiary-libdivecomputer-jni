/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#ifndef AUTOPARSER_H_
#define AUTOPARSER_H_

#include <iostream>
#include <libdivecomputer/parser.h>
#include <memory>
#include "Exception.h"
#include "DiveProfile.h"

/**
 * An object of this class is responsible for parsing of individual dives.
 * It is responsible for automatic resource management.
 */
class AutoParser {
private:
	dc_parser_t * parser;

	AutoParser(const AutoParser & other);
	AutoParser & operator=(const AutoParser & other);

	static void sample_cb(dc_sample_type_t type, dc_sample_value_t value, void * userdata);
	void parserInit(dc_device_t * device);
	void registerParserData(const unsigned char data[], unsigned int size);
	unsigned int getNumGases() const;
	unsigned int getNumTanks() const;

public:

	/**
	 * @param device Internal device representation.
	 * @param data Raw data.
	 * @param size Size of the data.
	 */
	AutoParser(dc_device_t * device, const unsigned char data[], unsigned int size);
	~AutoParser();

	/**
	 * @return Date and time of the beggining of the dive.
	 */
	dc_datetime_t getDateTime() const;

	/**
	 * @return Total length of the dive.
	 */
	unsigned int getDiveTime() const;

	/**
	 * @return Maximum depth reached during the dive.
	 */
	double getMaxDepth() const;

	/**
	 * @return Average depth during the dive.
	 */
	std::shared_ptr<double> getAvgDepth() const;

	/** @return Gas mixes used during the dive. */
	std::vector<dc_gasmix_t> getGasMixes() const;

	/** @return Tanks used during the dive. */
	std::vector<dc_tank_t> getTanks() const;

	/**
	 * @return Temperature at the bottom.
	 */
	std::shared_ptr<double> getBottomTemperature() const;

	/**
	 * @return Dive profile.
	 */
	DiveProfile getDiveProfile() const;
};

#endif
