/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#ifndef _Included_core_DeviceConnector
#define _Included_core_DeviceConnector

#include <jni.h>
#include <vector>
#include <set>
#include <exception>
#include <math.h>
#include <algorithm>
#include <memory>
#include <libdivecomputer/descriptor.h>
#include "device_info_t.h"
#include "dive_data_t.h"
#include "AutoConnector.h"
#include "AutoDevice.h"
#include "Dive.h"
#include "DiveProfile.h"
#include "Exception.h"
#include "CommunicationException.h"
#include "UnknownDeviceException.h"

namespace {

/**
 * Format a given time object as a date-time string "YYYY-MM-DD HH:MM:SS".
 */
const char * dateTimeToTimestamp(const dc_datetime_t & dateTime, char (&buffer)[20]);

jint roundToInt(const double value);

/**
 * Return a copy of the given collection with all negative numbers (including -0.0) replaced by 0.0.
 */
std::vector<std::shared_ptr<float>> replaceNegativeWithZero(std::vector<std::shared_ptr<float>> & nativePointer);

/**
 * Handle an exception thrown from the Java code.
 * Obtain the cause if any and wrap it in a given wrapper exception type
 * attaching a short message with extra information from the native code.
 */
jobject handleJavaException(JNIEnv * java, jclass wrapperClazz, jmethodID wrapperConstructor, const char * message);

/**
 * Wrap a given short message in a Java exception type and throw it.
 */
jobject throwExceptionMessage(JNIEnv * java, jclass wrapperClazz, const char * message);

/**
 * Wrap a given Java throwable in an exception type.
 * This function should never fail.
 * Return a wrapped throwable or a NULL pointer in case of failure.
 */
jthrowable wrapJavaThrowable(JNIEnv * java, jclass wrapperClazz, jmethodID wrapperConstructor, const char * message, jthrowable cause);

template<typename T>
bool setObjectArrayElement(JNIEnv * java, jobjectArray container, jsize index, jclass clazz, jmethodID constructor, const std::shared_ptr<T> & nativePointer);

template<typename T>
bool setObjectArrayElement(JNIEnv * java, jobjectArray container, jsize index, jclass clazz, jmethodID constructor, const std::shared_ptr<T> & nativePointer) {
	java->SetObjectArrayElement(container, index, (nativePointer != 0) ? java->NewObject(clazz, constructor, *nativePointer) : 0);
	return java->ExceptionCheck();
}

}

#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     myscubadiary_main_java_core_DeviceConnector
 * Method:    loadUserDivesFromDevice
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/SortedSet;)Lmyscubadiary/main/java/core/Device;
 */
JNIEXPORT jobject JNICALL Java_myscubadiary_main_java_core_DeviceConnector_loadUserDivesFromDevice
  (JNIEnv *, jclass, jstring, jstring, jstring, jobject);

/*
 * Class:     myscubadiary_main_java_core_DeviceConnector
 * Method:    getSupportedDevices
 * Signature: ()Ljava/util/SortedSet;
 */
JNIEXPORT jobject JNICALL Java_myscubadiary_main_java_core_DeviceConnector_getSupportedDevices
  (JNIEnv *, jclass);

/*
 * Class:     myscubadiary_main_java_core_DeviceConnector
 * Method:    interruptDeviceDownload
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_myscubadiary_main_java_core_DeviceConnector_interruptDeviceDownload
  (JNIEnv *, jclass);

#ifdef __cplusplus
}
#endif
#endif
