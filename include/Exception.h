/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#ifndef EXCEPTION_H_
#define EXCEPTION_H_

#include <string>
#include <exception>

class Exception : public std::exception {
private:
	const std::string message;

public:
	Exception(const std::string & message);

	Exception(const std::string & message, const int errorCode);

	virtual const char * what() const throw();
};

#endif
