/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#ifndef AUTOCONNECTOR_H_
#define AUTOCONNECTOR_H_

#include <signal.h>
#include <vector>
#include <utility>
#include "Exception.h"
#include "UnknownDeviceException.h"
#include "device_info_t.h"
#include "Utility.h"

/**
 * An object of this class represents a connection to a given device.
 * It is responsible for automatic resource management.
 */
class AutoConnector {
private:
	const std::string name;
	const char * fingerprint;

	AutoConnector(const AutoConnector & other);
	AutoConnector & operator=(const AutoConnector & other);

public:
	const char * port;
	dc_context_t * context;
	dc_descriptor_t * descriptor;
	dc_buffer_t * fp;

	/**
	 * Return a list of vendor and product names of all supported devices.
	 */
	static std::vector<device_info_t> listSupportedDevices();

	/**
	 * @param devicePort Device connection port.
	 * @param vendorName Device vendor's name.
	 * @param deviceName Name of the device.
	 */
	AutoConnector(const char * devicePort, const char * vendorName, const char * deviceName);
	~AutoConnector();
};

#endif
