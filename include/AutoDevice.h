/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#ifndef AUTODEVICE_H_
#define AUTODEVICE_H_

#include <iostream>
#include <libdivecomputer/context.h>
#include <libdivecomputer/descriptor.h>
#include <libdivecomputer/buffer.h>
#include <libdivecomputer/device.h>
#include "dive_data_t.h"
#include "device_data_t.h"
#include "Utility.h"
#include "AutoConnector.h"
#include "AutoParser.h"
#include "Dive.h"
#include "Exception.h"
#include "CommunicationException.h"

/**
 * An object of this class represents a device (dive computer).
 * It is responsible for automatic resource management.
 */
class AutoDevice {
private:
	static bool wasInterrupted;
	dc_device_t * device;
	dc_descriptor_t * deviceDescriptor;
	char serial[50];
	device_data_t deviceData;

	static const char * g_cachedir;
	static int g_cachedir_read;

	static void sighandler(int signum);
	static int cancel_cb(void * userdata);
	static void event_cb(dc_device_t * device, dc_event_type_t event, const void * data, void * userdata);
	static int dive_cb(const unsigned char * data, unsigned int size, const unsigned char * fingerprint, unsigned int fsize, void * userdata);

	AutoDevice(const AutoDevice & other);
	AutoDevice & operator=(const AutoDevice & other);

	void open(dc_context_t * context, dc_descriptor_t * descriptor, const char * devname);
	void registerEventHandler();
	void registerCancelHandler();
	void registerFingerprintData(dc_buffer_t * fingerprint);

public:

	/**
	 * @param connector Device connector.
	 */
	AutoDevice(const AutoConnector & connector);
	~AutoDevice();

	/**
	 * Download all dives from the device.
	 * @return Contents of the device's memory.
	 */
	std::auto_ptr<dive_data_t> downloadDives();

	/**
	 * @return Serial number of the device.
	 */
	const char * getSerial() const;

	/**
	 * Interrupt the download process.
	 */
	void interrupt();
};

#endif
