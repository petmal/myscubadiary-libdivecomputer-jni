/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

#ifndef DIVE_DATA_T_H_
#define DIVE_DATA_T_H_

#include <vector>
#include <libdivecomputer/device.h>
#include "Dive.h"

/**
 * This class represents a container for data downloaded from a given device.
 * It is responsible for automatic resource management.
 */
class dive_data_t {
private:
	dive_data_t(const dive_data_t & other);
	dive_data_t & operator=(const dive_data_t & other);

public:
	dc_device_t * device;
	unsigned int number;
	dc_buffer_t * fingerprint;
	std::vector<Dive> logbook;

	/**
	 * @param device Device to be downloaded.
	 */
	dive_data_t(dc_device_t * device);
	~dive_data_t();
};

#endif